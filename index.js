const express = require("express");
const path = require("path");
const fs = require("fs");
const router = express.Router();
const morgan = require("morgan");
const loggerMiddleware = morgan("tiny");

const app = express();
app.use(express.json());

app
  .use(loggerMiddleware)
  .post("/api/files", (req, res) => {
    const { filename, content } = req.body;
    const extSupport = [".log", ".txt", ".json", ".yaml", ".xml", ".xml"];
    const extension = path.extname(filename);

    const fileArray = fs.readdirSync("./files");
    const filesName = fileArray.find((file) => file === filename);
    const fileNameSupport = extSupport.find((ext) => ext === extension);

    if (!fileNameSupport) {
      return res
        .status(400)
        .json({ message: "Please add correct file name extension" });
    }

    if (filesName) {
      return res
        .status(409)
        .json({ message: "File with this name already exists" });
    }

    if (!content) {
      return res
        .status(400)
        .json({ message: "Please specify 'content' parameter" });
    }

    fs.writeFile(path.join(__dirname, "/files", filename), content, (err) => {
      if (err) {
        return res.status(500).json({ message: '"Server error"' });
      } else {
        return res
          .status(200)
          .json({ message: "File created successfully", filename });
      }
    });
  })
  .get("/api/files", (req, res) => {
    fs.readdir("./files", (err, files) => {
      if (err) {
        return res.status(500).json({ message: "Server error" });
      }
      if (files.length === 0) {
        return res.status(400).json({ message: "Client error" });
      }
      return res.status(200).json({ message: "Success", files });
    });
  })
  .get("/api/files/:filename", (req, res) => {
    const { filename } = req.params;
    const extension = path.extname(filename).substring(1);
    const fileArray = fs.readdirSync("./files");
    const filesName = fileArray.find((file) => file === filename);

    if (!filesName) {
      return res
        .status(400)
        .json({ message: `No file with ${filename} filename found` });
    }

    fs.readFile(
      path.join(__dirname, "files", filename),
      "utf-8",
      (err, content) => {
        if (err) {
          return res.status(500).json({ message: "Server error" });
        }

        fs.stat(path.join(__dirname, "files", filename), (statError, stat) => {
          return res.status(200).json({
            message: "Success",
            filename,
            content,
            extension,
            uploadedDate: stat ? stat.birthtime : statError,
          });
        });
      }
    );
  });

app.listen(8080, () => {
  if (!fs.existsSync("files")) {
    fs.mkdirSync("files");
  }
  console.log("Server works at port 8080!");
});
